#pragma once

#include <QQmlComponent>
#include <QQmlEngine>
#include <QQuickItem>

#include <QtQmlModels/private/qqmldelegatemodel_p.h>
#include <QtQuick/private/qquicktransition_p.h>

class ExpoView;
class ExpoViewDisplaceTransitionManager;

class ExpoViewDelegate : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(int naturalX READ naturalX WRITE setNaturalX NOTIFY naturalXChanged)
    Q_PROPERTY(int naturalY READ naturalY WRITE setNaturalY NOTIFY naturalYChanged)
    Q_PROPERTY(int naturalWidth READ naturalWidth WRITE setNaturalWidth NOTIFY naturalWidthChanged)
    Q_PROPERTY(int naturalHeight READ naturalHeight WRITE setNaturalHeight NOTIFY naturalHeightChanged)
    Q_PROPERTY(QString persistentKey READ persistentKey WRITE setPersistentKey NOTIFY persistentKeyChanged)

public:
    explicit ExpoViewDelegate(QQuickItem *parent = nullptr);
    ~ExpoViewDelegate() override;

    ExpoView *view() const;
    void setView(ExpoView *view);

    bool polished() const;
    void setPolished(bool polished);

    bool isEnabled() const;
    void setEnabled(bool enabled);

    int naturalX() const;
    void setNaturalX(int x);

    int naturalY() const;
    void setNaturalY(int y);

    int naturalWidth() const;
    void setNaturalWidth(int width);

    int naturalHeight() const;
    void setNaturalHeight(int height);

    QRect naturalRect() const;
    QMargins margins() const;

    QString persistentKey() const;
    void setPersistentKey(const QString &key);

    void transitionTo(QQuickTransition *transition, const QRectF &rect);

    void scheduleLayout();

Q_SIGNALS:
    void enabledChanged();
    void naturalXChanged();
    void naturalYChanged();
    void naturalWidthChanged();
    void naturalHeightChanged();
    void persistentKeyChanged();

private:
    QString m_persistentKey;
    bool m_enabled = true;
    bool m_polished = false;
    int m_naturalX = 0;
    int m_naturalY = 0;
    int m_naturalWidth = 0;
    int m_naturalHeight = 0;
    std::optional<int> m_x;
    std::optional<int> m_y;
    std::optional<int> m_width;
    std::optional<int> m_height;
    QPointer<ExpoView> m_view;
    QScopedPointer<ExpoViewDisplaceTransitionManager> m_transitionManager;
};

class ExpoView : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(LayoutMode mode READ mode WRITE setMode NOTIFY modeChanged)
    Q_PROPERTY(bool organized READ organized WRITE setOrganized NOTIFY organizedChanged)
    Q_PROPERTY(bool fillGaps READ fillGaps WRITE setFillGaps NOTIFY fillGapsChanged)
    Q_PROPERTY(int spacing READ spacing WRITE setSpacing NOTIFY spacingChanged)
    Q_PROPERTY(QQmlComponent *delegate READ delegate WRITE setDelegate NOTIFY delegateChanged)
    Q_PROPERTY(QVariant model READ model WRITE setModel NOTIFY modelChanged)
    Q_PROPERTY(QQuickTransition *displaced READ displacedTransition WRITE setDisplacedTransition NOTIFY displacedTransitionChanged)
    QML_ELEMENT

public:
    enum LayoutMode : uint {
        LayoutClosest = 0,
        LayoutNatural = 1,
    };
    Q_ENUM(LayoutMode)

    explicit ExpoView(QQuickItem *parent = nullptr);
    ~ExpoView() override;

    LayoutMode mode() const;
    void setMode(LayoutMode mode);

    bool polished() const;
    bool organized() const;
    void setOrganized(bool organized);

    bool fillGaps() const;
    void setFillGaps(bool fill);

    int spacing() const;
    void setSpacing(int spacing);

    QQmlComponent *delegate() const;
    void setDelegate(QQmlComponent *delegate);

    QVariant model() const;
    void setModel(const QVariant &model);

    QQuickTransition *displacedTransition() const;
    void setDisplacedTransition(QQuickTransition *displaced);

    void enableItem(ExpoViewDelegate *item);
    void disableItem(ExpoViewDelegate *item);

    void componentComplete() override;

protected:
    void geometryChange(const QRectF &newGeometry, const QRectF &oldGeometry) override;
    void updatePolish() override;

Q_SIGNALS:
    void modeChanged();
    void organizedChanged();
    void fillGapsChanged();
    void spacingChanged();
    void delegateChanged();
    void modelChanged();
    void displacedTransitionChanged();

private Q_SLOTS:
    void modelUpdated(const QQmlChangeSet &changeSet, bool reset);
    void initItem(int index, QObject *object);

private:
    void calculateWindowTransformationsClosest();
    void calculateWindowTransformationsNatural();
    void resetTransformations();

    void clear();
    void refill();

    LayoutMode m_mode = LayoutNatural;

    QVariant m_modelVariant;
    QScopedPointer<QQmlDelegateModel> m_model;

    QQmlComponent *m_delegate = nullptr;
    QList<ExpoViewDelegate *> m_items;
    QList<ExpoViewDelegate *> m_activeItems;

    QQuickTransition *m_displacedTransition = nullptr;

    int m_accuracy = 20;
    int m_spacing = 10;
    bool m_organized = false;
    bool m_polished = false;
    bool m_fillGaps = false;
};
