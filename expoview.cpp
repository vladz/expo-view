#include "expoview.h"

#include <QtQuick/private/qquicktransitionmanager_p_p.h>

class ExpoViewDisplaceTransitionManager : public QQuickTransitionManager
{
public:
    void transition(ExpoViewDelegate *item, QQuickTransition *transition, const QRectF &geometry)
    {
        const QList<QQuickStateAction> actions {
            QQuickStateAction(item, QLatin1String("x"), geometry.x()),
            QQuickStateAction(item, QLatin1String("y"), geometry.y()),
            QQuickStateAction(item, QLatin1String("width"), geometry.width()),
            QQuickStateAction(item, QLatin1String("height"), geometry.height()),
        };

        QQuickTransitionManager::transition(actions, transition, item);
    }
};

ExpoViewDelegate::ExpoViewDelegate(QQuickItem *parent)
    : QQuickItem(parent)
{
}

ExpoViewDelegate::~ExpoViewDelegate()
{
}

ExpoView *ExpoViewDelegate::view() const
{
    return m_view;
}

void ExpoViewDelegate::setView(ExpoView *view)
{
    m_view = view;
}

bool ExpoViewDelegate::isEnabled() const
{
    return m_enabled;
}

void ExpoViewDelegate::setEnabled(bool enabled)
{
    if (m_enabled != enabled) {
        m_enabled = enabled;
        if (isComponentComplete()) {
            if (enabled) {
                m_view->enableItem(this);
            } else {
                m_view->disableItem(this);
            }
        }
        Q_EMIT enabledChanged();
    }
}

bool ExpoViewDelegate::polished() const
{
    return m_polished;
}

void ExpoViewDelegate::setPolished(bool polished)
{
    m_polished = polished;
}

int ExpoViewDelegate::naturalX() const
{
    return m_naturalX;
}

void ExpoViewDelegate::setNaturalX(int x)
{
    if (m_naturalX != x) {
        m_naturalX = x;
        if (!m_enabled || !m_polished) {
            setX(x);
        }
        if (isComponentComplete()) {
            scheduleLayout();
        }
        Q_EMIT naturalXChanged();
    }
}

int ExpoViewDelegate::naturalY() const
{
    return m_naturalY;
}

void ExpoViewDelegate::setNaturalY(int y)
{
    if (m_naturalY != y) {
        m_naturalY = y;
        if (!m_enabled || !m_polished) {
            setY(y);
        }
        if (isComponentComplete()) {
            scheduleLayout();
        }
        Q_EMIT naturalYChanged();
    }
}

int ExpoViewDelegate::naturalWidth() const
{
    return m_naturalWidth;
}

void ExpoViewDelegate::setNaturalWidth(int width)
{
    if (m_naturalWidth != width) {
        m_naturalWidth = width;
        if (!m_enabled || !m_polished) {
            setWidth(width);
        }
        if (isComponentComplete()) {
            scheduleLayout();
        }
        Q_EMIT naturalWidthChanged();
    }
}

int ExpoViewDelegate::naturalHeight() const
{
    return m_naturalHeight;
}

void ExpoViewDelegate::setNaturalHeight(int height)
{
    if (m_naturalHeight != height) {
        m_naturalHeight = height;
        if (!m_enabled || !m_polished) {
            setHeight(height);
        }
        if (isComponentComplete()) {
            scheduleLayout();
        }
        Q_EMIT naturalHeightChanged();
    }
}

QRect ExpoViewDelegate::naturalRect() const
{
    return QRect(m_naturalX, m_naturalY, m_naturalWidth, m_naturalHeight);
}

QMargins ExpoViewDelegate::margins() const
{
    return QMargins();
}

QString ExpoViewDelegate::persistentKey() const
{
    return m_persistentKey;
}

void ExpoViewDelegate::setPersistentKey(const QString &key)
{
    if (m_persistentKey != key) {
        m_persistentKey = key;
        Q_EMIT persistentKeyChanged();
    }
}

void ExpoViewDelegate::transitionTo(QQuickTransition *transition, const QRectF &rect)
{
    if (position() == rect.topLeft() && size() == rect.size()) {
        return;
    }

    const bool skipTransition = m_view->polished() != m_polished;
    if (!transition || skipTransition) {
        setPosition(rect.topLeft());
        setSize(rect.size());
    } else {
        if (!m_transitionManager) {
            m_transitionManager.reset(new ExpoViewDisplaceTransitionManager);
        }
        m_transitionManager->transition(this, transition, rect);
    }
}

void ExpoViewDelegate::scheduleLayout()
{
    if (m_enabled) {
        m_view->polish();
    }
}

ExpoView::ExpoView(QQuickItem *parent)
    : QQuickItem(parent)
{
}

ExpoView::~ExpoView()
{
    clear();
}

ExpoView::LayoutMode ExpoView::mode() const
{
    return m_mode;
}

void ExpoView::setMode(LayoutMode mode)
{
    if (m_mode != mode) {
        m_mode = mode;
        polish();
        Q_EMIT modeChanged();
    }
}

bool ExpoView::polished() const
{
    return m_polished;
}

bool ExpoView::organized() const
{
    return m_organized;
}

void ExpoView::setOrganized(bool organized)
{
    if (m_organized != organized) {
        m_organized = organized;
        polish();
        Q_EMIT organizedChanged();
    }
}

bool ExpoView::fillGaps() const
{
    return m_fillGaps;
}

void ExpoView::setFillGaps(bool fill)
{
    if (m_fillGaps != fill) {
        m_fillGaps = fill;
        polish();
        Q_EMIT fillGapsChanged();
    }
}

int ExpoView::spacing() const
{
    return m_spacing;
}

void ExpoView::setSpacing(int spacing)
{
    if (m_spacing != spacing) {
        m_spacing = spacing;
        polish();
        Q_EMIT spacingChanged();
    }
}

QQmlComponent *ExpoView::delegate() const
{
    return m_delegate;
}

void ExpoView::setDelegate(QQmlComponent *delegate)
{
    if (m_delegate != delegate) {
        m_delegate = delegate;
        if (m_model) {
            m_model->setDelegate(delegate);
        }
        Q_EMIT delegateChanged();
    }
}

QVariant ExpoView::model() const
{
    return m_modelVariant;
}

void ExpoView::setModel(const QVariant &model)
{
    if (m_modelVariant == model) {
        return;
    }

    if (auto object = qvariant_cast<QObject *>(model)) {
        if (auto instanceModel = qobject_cast<QQmlInstanceModel *>(object)) {
            qWarning() << "Unsupported instance model type:" << instanceModel;
            return;
        }
    }

    if (m_model) {
        disconnect(m_model.get(), &QQmlDelegateModel::modelUpdated, this, &ExpoView::modelUpdated);
        disconnect(m_model.get(), &QQmlDelegateModel::initItem, this, &ExpoView::initItem);
        clear();
    }

    m_modelVariant = model;
    m_model.reset(new QQmlDelegateModel(qmlContext(this)));
    m_model->setDelegate(m_delegate);
    m_model->setModel(model);

    if (m_model) {
        connect(m_model.get(), &QQmlDelegateModel::modelUpdated, this, &ExpoView::modelUpdated);
        connect(m_model.get(), &QQmlDelegateModel::initItem, this, &ExpoView::initItem);
        refill();
    }

    Q_EMIT modelChanged();
}

void ExpoView::clear()
{
    if (!isComponentComplete()) {
        return;
    }

    for (QQuickItem *item : std::as_const(m_items)) {
        m_model->release(item);
    }

    m_items.clear();
    m_activeItems.clear();
}

void ExpoView::enableItem(ExpoViewDelegate *item)
{
    m_activeItems.append(item);
    polish();
}

void ExpoView::disableItem(ExpoViewDelegate *item)
{
    m_activeItems.removeOne(item);
    item->transitionTo(m_displacedTransition, item->naturalRect());
    polish();
}

void ExpoView::refill()
{
    if (!isComponentComplete()) {
        return;
    }

    QList<ExpoViewDelegate *> items;
    for (int index = 0; index < m_model->count(); ++index) {
        if (QObject *object = m_model->object(index)) {
            ExpoViewDelegate *item = qobject_cast<ExpoViewDelegate *>(object);
            if (item) {
                items.append(item);
            } else {
                m_model->release(object);
            }
        }
    }

    const QList<ExpoViewDelegate *> oldItems = m_items;
    m_items = items;

    m_activeItems.clear();
    m_activeItems.reserve(m_items.size());
    for (ExpoViewDelegate *item : std::as_const(m_items)) {
        if (item->isEnabled()) {
            m_activeItems.append(item);
        }
    }

    for (ExpoViewDelegate *item : std::as_const(oldItems)) {
        m_model->release(item);
    }

    polish();
}

void ExpoView::modelUpdated(const QQmlChangeSet &changeSet, bool reset)
{
    Q_UNUSED(changeSet)
    Q_UNUSED(reset)

    if (isComponentComplete()) {
        if (!changeSet.inserts().isEmpty() || !changeSet.removes().isEmpty()) {
            refill();
        }
    }
}

void ExpoView::initItem(int index, QObject *object)
{
    ExpoViewDelegate *item = qobject_cast<ExpoViewDelegate *>(object);
    if (item) {
        item->setView(this);
        item->setParentItem(this);
    } else {
        qWarning() << object << "at index" << index << "has invalid type. ExpoViewDelegate is expected";
    }
}

QQuickTransition *ExpoView::displacedTransition() const
{
    return m_displacedTransition;
}

void ExpoView::setDisplacedTransition(QQuickTransition *displaced)
{
    if (m_displacedTransition != displaced) {
        m_displacedTransition = displaced;
        Q_EMIT displacedTransitionChanged();
    }
}

void ExpoView::componentComplete()
{
    if (m_model) {
        m_model->componentComplete();
    }

    QQuickItem::componentComplete();

    refill();
}

void ExpoView::geometryChange(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    if (newGeometry.size() != oldGeometry.size()) {
        polish();
    }
    QQuickItem::geometryChange(newGeometry, oldGeometry);
}

void ExpoView::updatePolish()
{
    if (m_organized) {
        switch (m_mode) {
        case LayoutClosest:
            calculateWindowTransformationsClosest();
            break;
        case LayoutNatural:
            calculateWindowTransformationsNatural();
            break;
        }
        m_polished = true;
    } else {
        resetTransformations();
        m_polished = false;
    }
}

static int distance(const QPoint &a, const QPoint &b)
{
    const int xdiff = a.x() - b.x();
    const int ydiff = a.y() - b.y();
    return int(std::sqrt(qreal(xdiff * xdiff + ydiff * ydiff)));
}

static QRect centered(ExpoViewDelegate *item, const QRect &bounds)
{
    const QSize scaled = QSize(item->naturalWidth(), item->naturalHeight())
                             .scaled(bounds.size(), Qt::KeepAspectRatio);

    return QRect(bounds.center().x() - scaled.width() / 2,
                 bounds.center().y() - scaled.height() / 2,
                 scaled.width(),
                 scaled.height());
}

void ExpoView::calculateWindowTransformationsClosest()
{
    if (m_activeItems.isEmpty()) {
        return;
    }
    QRect area = QRect(0, 0, width(), height());
    const int columns = int(std::ceil(std::sqrt(qreal(m_activeItems.count()))));
    const int rows = int(std::ceil(m_activeItems.count() / qreal(columns)));

    // Assign slots
    const int slotWidth = area.width() / columns;
    const int slotHeight = area.height() / rows;
    QVector<ExpoViewDelegate *> takenSlots;
    takenSlots.resize(rows * columns);
    takenSlots.fill(nullptr);

    // precalculate all slot centers
    QVector<QPoint> slotCenters;
    slotCenters.resize(rows * columns);
    for (int x = 0; x < columns; ++x) {
        for (int y = 0; y < rows; ++y) {
            slotCenters[x + y * columns] = QPoint(area.x() + slotWidth * x + slotWidth / 2,
                                                  area.y() + slotHeight * y + slotHeight / 2);
        }
    }

    // Assign each window to the closest available slot
    QList<ExpoViewDelegate *> tmpList = m_activeItems; // use a QLinkedList copy instead?
    while (!tmpList.isEmpty()) {
        ExpoViewDelegate *item = tmpList.first();
        int slotCandidate = -1, slotCandidateDistance = INT_MAX;
        const QPoint pos = item->naturalRect().center();

        for (int i = 0; i < columns * rows; ++i) { // all slots
            const int dist = distance(pos, slotCenters[i]);
            if (dist < slotCandidateDistance) { // window is interested in this slot
                ExpoViewDelegate *occupier = takenSlots[i];
                Q_ASSERT(occupier != item);
                if (!occupier || dist < distance(occupier->naturalRect().center(), slotCenters[i])) {
                    // either nobody lives here, or we're better - takeover the slot if it's our best
                    slotCandidate = i;
                    slotCandidateDistance = dist;
                }
            }
        }
        Q_ASSERT(slotCandidate != -1);
        if (takenSlots[slotCandidate]) {
            tmpList << takenSlots[slotCandidate]; // occupier needs a new home now :p
        }
        tmpList.removeAll(item);
        takenSlots[slotCandidate] = item; // ...and we rumble in =)
    }

    for (int slot = 0; slot < columns * rows; ++slot) {
        ExpoViewDelegate *item = takenSlots[slot];
        if (!item) { // some slots might be empty
            continue;
        }

        // Work out where the slot is
        QRect target(area.x() + (slot % columns) * slotWidth,
                     area.y() + (slot / columns) * slotHeight,
                     slotWidth, slotHeight);
        target.adjust(m_spacing, m_spacing, -m_spacing, -m_spacing); // Borders
        target = target.marginsRemoved(item->margins());

        qreal scale;
        if (target.width() / qreal(item->naturalWidth()) < target.height() / qreal(item->naturalHeight())) {
            // Center vertically
            scale = target.width() / qreal(item->naturalWidth());
            target.moveTop(target.top() + (target.height() - int(item->naturalHeight() * scale)) / 2);
            target.setHeight(int(item->naturalHeight() * scale));
        } else {
            // Center horizontally
            scale = target.height() / qreal(item->naturalHeight());
            target.moveLeft(target.left() + (target.width() - int(item->naturalWidth() * scale)) / 2);
            target.setWidth(int(item->naturalWidth() * scale));
        }
        // Don't scale the windows too much
        if (scale > 2.0 || (scale > 1.0 && (item->naturalWidth() > 300 || item->naturalHeight() > 300))) {
            scale = (item->naturalWidth() > 300 || item->naturalHeight() > 300) ? 1.0 : 2.0;
            target = QRect(
                target.center().x() - int(item->naturalWidth() * scale) / 2,
                target.center().y() - int(item->naturalHeight() * scale) / 2,
                scale * item->naturalWidth(), scale * item->naturalHeight());
        }

        item->transitionTo(m_displacedTransition, target);
        item->setPolished(true);
    }
}

static inline int heightForWidth(ExpoViewDelegate *item, int width)
{
    return int((width / qreal(item->naturalWidth())) * item->naturalHeight());
}

static bool isOverlappingAny(ExpoViewDelegate *w, const QHash<ExpoViewDelegate *, QRect> &targets, const QRegion &border, int spacing)
{
    QHash<ExpoViewDelegate *, QRect>::const_iterator winTarget = targets.find(w);
    if (winTarget == targets.constEnd()) {
        return false;
    }
    if (border.intersects(*winTarget)) {
        return true;
    }
    const QMargins halfSpacing(spacing / 2, spacing / 2, spacing / 2, spacing / 2);

    // Is there a better way to do this?
    QHash<ExpoViewDelegate *, QRect>::const_iterator target;
    for (target = targets.constBegin(); target != targets.constEnd(); ++target) {
        if (target == winTarget) {
            continue;
        }
        if (winTarget->marginsAdded(halfSpacing).intersects(target->marginsAdded(halfSpacing))) {
            return true;
        }
    }
    return false;
}

void ExpoView::calculateWindowTransformationsNatural()
{
    if (m_activeItems.isEmpty()) {
        return;
    }
    const QRect area = QRect(0, 0, width(), height());

    // As we are using pseudo-random movement (See "slot") we need to make sure the list
    // is always sorted the same way no matter which window is currently active.
    std::sort(m_activeItems.begin(), m_activeItems.end(), [](const ExpoViewDelegate *a, const ExpoViewDelegate *b) {
        return a->persistentKey() < b->persistentKey();
    });

    QRect bounds;
    int direction = 0;
    QHash<ExpoViewDelegate *, QRect> targets;
    QHash<ExpoViewDelegate *, int> directions;

    for (ExpoViewDelegate *item : std::as_const(m_activeItems)) {
        const QRect itemRect(item->naturalX(), item->naturalY(), item->naturalWidth(), item->naturalHeight());
        targets[item] = itemRect;
        // Reuse the unused "slot" as a preferred direction attribute. This is used when the window
        // is on the edge of the screen to try to use as much screen real estate as possible.
        directions[item] = direction;
        bounds = bounds.united(itemRect);
        direction++;
        if (direction == 4) {
            direction = 0;
        }
    }

    // Iterate over all windows, if two overlap push them apart _slightly_ as we try to
    // brute-force the most optimal positions over many iterations.
    const int halfSpacing = m_spacing / 2;
    bool overlap;
    do {
        overlap = false;
        for (ExpoViewDelegate *item : std::as_const(m_activeItems)) {
            QRect *target_w = &targets[item];
            for (ExpoViewDelegate *e : std::as_const(m_activeItems)) {
                if (item == e) {
                    continue;
                }

                QRect *target_e = &targets[e];
                if (target_w->adjusted(-halfSpacing, -halfSpacing, halfSpacing, halfSpacing)
                        .intersects(target_e->adjusted(-halfSpacing, -halfSpacing, halfSpacing, halfSpacing))) {
                    overlap = true;

                    // Determine pushing direction
                    QPoint diff(target_e->center() - target_w->center());
                    // Prevent dividing by zero and non-movement
                    if (diff.x() == 0 && diff.y() == 0) {
                        diff.setX(1);
                    }
                    // Try to keep screen aspect ratio
                    // if (bounds.height() / bounds.width() > area.height() / area.width())
                    //    diff.setY(diff.y() / 2);
                    // else
                    //    diff.setX(diff.x() / 2);
                    // Approximate a vector of between 10px and 20px in magnitude in the same direction
                    diff *= m_accuracy / qreal(diff.manhattanLength());
                    // Move both windows apart
                    target_w->translate(-diff);
                    target_e->translate(diff);

                    // Try to keep the bounding rect the same aspect as the screen so that more
                    // screen real estate is utilised. We do this by splitting the screen into nine
                    // equal sections, if the window center is in any of the corner sections pull the
                    // window towards the outer corner. If it is in any of the other edge sections
                    // alternate between each corner on that edge. We don't want to determine it
                    // randomly as it will not produce consistant locations when using the filter.
                    // Only move one window so we don't cause large amounts of unnecessary zooming
                    // in some situations. We need to do this even when expanding later just in case
                    // all windows are the same size.
                    // (We are using an old bounding rect for this, hopefully it doesn't matter)
                    int xSection = (target_w->x() - bounds.x()) / (bounds.width() / 3);
                    int ySection = (target_w->y() - bounds.y()) / (bounds.height() / 3);
                    diff = QPoint(0, 0);
                    if (xSection != 1 || ySection != 1) { // Remove this if you want the center to pull as well
                        if (xSection == 1) {
                            xSection = (directions[item] / 2 ? 2 : 0);
                        }
                        if (ySection == 1) {
                            ySection = (directions[item] % 2 ? 2 : 0);
                        }
                    }
                    if (xSection == 0 && ySection == 0) {
                        diff = QPoint(bounds.topLeft() - target_w->center());
                    }
                    if (xSection == 2 && ySection == 0) {
                        diff = QPoint(bounds.topRight() - target_w->center());
                    }
                    if (xSection == 2 && ySection == 2) {
                        diff = QPoint(bounds.bottomRight() - target_w->center());
                    }
                    if (xSection == 0 && ySection == 2) {
                        diff = QPoint(bounds.bottomLeft() - target_w->center());
                    }
                    if (diff.x() != 0 || diff.y() != 0) {
                        diff *= m_accuracy / qreal(diff.manhattanLength());
                        target_w->translate(diff);
                    }

                    // Update bounding rect
                    bounds = bounds.united(*target_w);
                    bounds = bounds.united(*target_e);
                }
            }
        }
    } while (overlap);

    // Compute the scale factor so the bounding rect fits the target area.
    qreal scale;
    if (bounds.width() <= area.width() && bounds.height() <= area.height()) {
        scale = 1.0;
    } else if (area.width() / qreal(bounds.width()) < area.height() / qreal(bounds.height())) {
        scale = area.width() / qreal(bounds.width());
    } else {
        scale = area.height() / qreal(bounds.height());
    }
    // Make bounding rect fill the screen size for later steps
    bounds = QRect(bounds.x() - (area.width() / scale - bounds.width()) / 2,
                   bounds.y() - (area.height() / scale - bounds.height()) / 2,
                   area.width() / scale,
                   area.height() / scale);

    // Move all windows back onto the screen and set their scale
    QHash<ExpoViewDelegate *, QRect>::iterator target = targets.begin();
    while (target != targets.end()) {
        target->setRect((target->x() - bounds.x()) * scale + area.x(),
                        (target->y() - bounds.y()) * scale + area.y(),
                        target->width() * scale,
                        target->height() * scale);
        ++target;
    }

    // Try to fill the gaps by enlarging windows if they have the space
    if (m_fillGaps) {
        // Don't expand onto or over the border
        QRegion borderRegion(area.adjusted(-200, -200, 200, 200));
        borderRegion ^= area;

        bool moved;
        do {
            moved = false;
            for (ExpoViewDelegate *item : std::as_const(m_activeItems)) {
                QRect oldRect;
                QRect *target = &targets[item];
                // This may cause some slight distortion if the windows are enlarged a large amount
                int widthDiff = m_accuracy;
                int heightDiff = heightForWidth(item, target->width() + widthDiff) - target->height();
                int xDiff = widthDiff / 2; // Also move a bit in the direction of the enlarge, allows the
                int yDiff = heightDiff / 2; // center windows to be enlarged if there is gaps on the side.

                // heightDiff (and yDiff) will be re-computed after each successful enlargement attempt
                // so that the error introduced in the window's aspect ratio is minimized

                // Attempt enlarging to the top-right
                oldRect = *target;
                target->setRect(target->x() + xDiff,
                                target->y() - yDiff - heightDiff,
                                target->width() + widthDiff,
                                target->height() + heightDiff);
                if (isOverlappingAny(item, targets, borderRegion, m_spacing)) {
                    *target = oldRect;
                } else {
                    moved = true;
                    heightDiff = heightForWidth(item, target->width() + widthDiff) - target->height();
                    yDiff = heightDiff / 2;
                }

                // Attempt enlarging to the bottom-right
                oldRect = *target;
                target->setRect(target->x() + xDiff,
                                target->y() + yDiff,
                                target->width() + widthDiff,
                                target->height() + heightDiff);
                if (isOverlappingAny(item, targets, borderRegion, m_spacing)) {
                    *target = oldRect;
                } else {
                    moved = true;
                    heightDiff = heightForWidth(item, target->width() + widthDiff) - target->height();
                    yDiff = heightDiff / 2;
                }

                // Attempt enlarging to the bottom-left
                oldRect = *target;
                target->setRect(target->x() - xDiff - widthDiff,
                                target->y() + yDiff,
                                target->width() + widthDiff,
                                target->height() + heightDiff);
                if (isOverlappingAny(item, targets, borderRegion, m_spacing)) {
                    *target = oldRect;
                } else {
                    moved = true;
                    heightDiff = heightForWidth(item, target->width() + widthDiff) - target->height();
                    yDiff = heightDiff / 2;
                }

                // Attempt enlarging to the top-left
                oldRect = *target;
                target->setRect(target->x() - xDiff - widthDiff,
                                target->y() - yDiff - heightDiff,
                                target->width() + widthDiff,
                                target->height() + heightDiff);
                if (isOverlappingAny(item, targets, borderRegion, m_spacing)) {
                    *target = oldRect;
                } else {
                    moved = true;
                }
            }
        } while (moved);

        // The expanding code above can actually enlarge windows over 1.0/2.0 scale, we don't like this
        // We can't add this to the loop above as it would cause a never-ending loop so we have to make
        // do with the less-than-optimal space usage with using this method.
        for (ExpoViewDelegate *item : std::as_const(m_activeItems)) {
            QRect *target = &targets[item];
            qreal scale = target->width() / qreal(item->naturalWidth());
            if (scale > 2.0 || (scale > 1.0 && (item->naturalWidth() > 300 || item->naturalHeight() > 300))) {
                scale = (item->naturalWidth() > 300 || item->naturalHeight() > 300) ? 1.0 : 2.0;
                target->setRect(target->center().x() - int(item->naturalWidth() * scale) / 2,
                                target->center().y() - int(item->naturalHeight() * scale) / 2,
                                item->naturalWidth() * scale,
                                item->naturalHeight() * scale);
            }
        }
    }

    for (ExpoViewDelegate *item : std::as_const(m_activeItems)) {
        item->transitionTo(m_displacedTransition, centered(item, targets.value(item).marginsRemoved(item->margins())));
        item->setPolished(true);
    }
}

void ExpoView::resetTransformations()
{
    for (ExpoViewDelegate *item : std::as_const(m_activeItems)) {
        QQuickTransition *transition = nullptr;
        if (item->polished()) {
            transition = m_displacedTransition;
        }

        item->transitionTo(transition, item->naturalRect());
        item->setPolished(false);
    }
}
